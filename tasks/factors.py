def factors(number):
    # ==============
    mylist = []

    for i in range(1, number):
        if number % i == 0:
            mylist.append(i)
    return (mylist[1:10])


    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
